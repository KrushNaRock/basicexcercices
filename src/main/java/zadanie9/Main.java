package zadanie9;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner konsola = new Scanner(System.in);

        System.out.println("Podaj wage:");
        int waga = konsola.nextInt();
        System.out.println("Podaj wzrost:");
        int wzrost = konsola.nextInt();
        System.out.println("Podaj wiek:");
        int wiek = konsola.nextInt();

        if (wzrost > 150 && waga<180 ) {
            System.out.println("Można wejść na kolejkę.");
        }else {
            System.out.println("Nie można wejść na kolejkę.");
        }
        if (wiek < 10 && wiek > 80) {
            System.out.println("Nie można wejść na kolejkę z powodu wieku..");
        }else {
            System.out.println("Można wejść na kolejkę.");
        }
        if ( wzrost > 150 && wzrost < 220) {
            System.out.println("Można wejść na kolejkę.");
        }else {
            System.out.println("Nie można wejść na kolejkę z powodu wzrostu");
        }
        if ( waga > 180) {
            System.out.println("Osoba nie może wejsć, gdyż jest za ciężka.");
        }
        if ( wiek < 10 ) {
            System.out.println("Osoba jest za młoda.");
        }
        if (wiek > 80) {
            System.out.println("Osoba jest za stara.");
        }
    }
}
