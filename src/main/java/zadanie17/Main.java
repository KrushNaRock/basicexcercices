package zadanie17;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner konsola = new Scanner(System.in);
        System.out.println("Wpisz liczbę");
        int liczba = konsola.nextInt();
        int n = 1;
        while(n <= liczba) {
            System.out.println(n);
            n*=2;
        }
    }
}
