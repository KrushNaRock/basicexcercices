package zadanie31;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Scanner ciąg = new Scanner(System.in);
        System.out.println("Wpisz wyraz, aby sprawdzić czy jest palindromem");
        String original = ciąg.next();
        String reverse = "";
        int length = original.length();

        for ( int i = length - 1; i >= 0; i-- )
            reverse = reverse + original.charAt(i);

        if (original.equals(reverse))
            System.out.println("Wyraz jest palindromem.");
        else
            System.out.println("Wyraz nie jest palindromem.");
    }
}
