package zadanie6;

public class Main {

    public static void main(String[] args) {

        float matematyka = 3;
        float chemia = 4;
        float j_polski = 1;
        float j_angielski = 5;
        float wos = 5;
        float informatyka = 5;


        float global = ((matematyka + chemia + j_polski + j_angielski + wos + informatyka) / 6);
        System.out.println("średnia= " + global);

        float science = ((matematyka + chemia + informatyka) / 3);
        System.out.println("średnia z przedmiotów ścisłych= " + science);

        if (matematyka == 1 || chemia == 1 || informatyka == 1) {
            System.out.println("Ocena z przedmiotów ścisłych jest niesdostateczna.");
        }

        float human = ((j_angielski + j_polski + wos) / 3);
        System.out.println("średnia z przedmiotów humanistycznych= " + human);

        if (j_angielski == 1 || j_polski == 1 || wos == 1 || human == 1) {
            System.out.println("Ocena z przedmiotów humanistycznych jest niedostateczna.");
        }
    }
}
