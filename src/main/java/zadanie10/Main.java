package zadanie10;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner konsola = new Scanner(System.in);

        System.out.println("Podaj dochód");
        float dochód = konsola.nextInt();

        if (dochód <= 85528) {
            System.out.println( "Należy zapłacić " + (dochód * 0.18 - 556.02) +" podatku." );
        }else {
            System.out.println( "Należy zapłacić " + (14839.02+((dochód - 85528)*0.32))+ " podatku.");
        }
    }
}
